#coding:utf-8
import cgi, cgitb, scraping

cgitb.enable()
form = cgi.FieldStorage()
try :
    if form.getvalue("query"):
        query = str(form.getvalue("query"))
        if len(query) <= 256:
            description = scraping.main(query)
            result = f"""
                <section id="portfolio-details" class="portfolio-details">
                    <div class="container" data-aos="fade-up">
                        <div class="row gy-4">
                            <div class="col-lg-8">
                                <div class="slides-1 portfolio-details-slider swiper">
                                    <div class="swiper-wrapper align-items-center">
                                        <div class="swiper-slide">
                                            <img src="assets/img/app-1.jpg" alt="">
                                        </div>
                                    </div>
                                <div class="swiper-pagination"></div>
                            </div>
                        </div>

                        <div class="col-lg-4">
                            <div class="portfolio-description">
                                <h2>Description</h2>
                                <p>
                                    {description}
                                </p>
                            </div>
                        </div>
                    </div>
                    </div>
                </section>
            """
        else:
            result = """
                <h1 style="text-align: center;font-size: 10rem;margin-top: 5rem;">Error 404</h1>
            """ 
    else:
        result = """
            <h1 style="text-align: center;font-size: 10rem;margin-top: 5rem;">Error 404</h1>
        """
except :
    result = """
        <h1 style="text-align: center;font-size: 10rem;margin-top: 5rem;">Error 404</h1>
    """

print("Content-type: text/html; charset=utf-8\n")

html = f"""
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <title>http://antic.search-details</title>
        <meta content="" name="description">
        <meta content="" name="keywords">
        <link rel='icon' type='image/png' href='assets/img/favicon.png'>
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,600;1,700&family=Montserrat:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&family=Raleway:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&display=swap" rel="stylesheet">
        <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
        <link href="assets/vendor/aos/aos.css" rel="stylesheet">
        <link href="assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
        <link href="assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">
        <link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet">
        <link href="assets/css/main.css" rel="stylesheet">
    </head>
    <body>
        <header id="header" class="header d-flex align-items-center fixed-top">
            <div class="container-fluid container-xl d-flex align-items-center justify-content-between">
                <a href="index.py">
                    <img src='assets/img/logo.jpg' alt='' class='logo' style="border-radius: 50px; width: 100px; height: auto;">
                </a>
                <i class="mobile-nav-toggle mobile-nav-show bi bi-list"></i>
                <i class="mobile-nav-toggle mobile-nav-hide d-none bi bi-x"></i>
                <nav id="navbar" class="navbar">
                    <ul>
                        <li><a href="index.py">Search</a></li>
                    </ul>
                </nav>
            </div>
        </header>
        <main id="main">
            <div class="breadcrumbs d-flex align-items-center" style="background-image: url('assets/img/portfolio-header.jpg');">
                <div class="container position-relative d-flex flex-column align-items-center">
                    <h2>Do a Search on a Target</h2>
                    <ol>
                        <li><a href="index.py">Home</a></li>
                        <li>Details</li>
                    </ol>
                </div>
            </div>
            {result}
        </main>
        <a href="#" class="scroll-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>
        <div id="preloader"></div>
        <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="assets/vendor/aos/aos.js"></script>
        <script src="assets/vendor/glightbox/js/glightbox.min.js"></script>
        <script src="assets/vendor/swiper/swiper-bundle.min.js"></script>
        <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
        <script src="assets/vendor/php-email-form/validate.js"></script>
        <script src="assets/js/main.js"></script>
    </body>
</html>
"""
print(html)