#coding:utf-8
import http.server
import time

server = http.server.HTTPServer
handler = http.server.CGIHTTPRequestHandler
handler.cgi_directories = ["/"]
list_port = [80, 88, 87, 89, 86, 85, 81, 82, 83, 84, 90]
acces, i = False, 0

while acces == False:
    port = list_port[i]
    address = ("", port)
    try:
        httpd = server(address, handler)
        acces = True
    except:
        acces = False
    i += 1

msg = """
******************************************************************************************************
**                 _    _          _                         _    _   ____        _                 **
**   __ _ _ __ _  | |_ \_/ ____   \_/_ __ _  __   _____  ___| |_ \_// ___/  __ _ | |_  ____  _ __   **
**  / _` | '__. \ | __/| |/  __|  | | '__. \ \ \ / / _ \/ __| __/| |||  __ / _` || __//    \| '__|  **
** | (_| | |   | || \_ | |  (__   | | |   | | \ V /  __/\__ \ |_ | || () || (_| || |_|  ()  | |     **
**  \__,_|_|   |_| \___|_|\____|  |_|_|   |_|  \_/ \___||___/\__\|_|\____/ \__,_| \__|\____/|_|     **
**                                                                                                  **
** - The Antic Investigator V 1.0.0                                                                 **
** - Coded by Abdel Fadil YIAGNIGNI                                                                 **
** - Research Report                                                                                **
******************************************************************************************************
"""
time.sleep(5)
print(msg)
time.sleep(10)
print(f"Server started")
print(f"\t- Sercice : HTTP\n\t- Port {port}")

httpd.serve_forever()