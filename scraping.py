#coding:utf-8

# I - Importation des modules
import datetime, requests, newspaper, aspose.pdf
from googlesearch import search

# II - Fonction de scraper des urls
def url_scraping(query) :
    links = []
    for url in search(query, num_results=25, lang="en", sleep_interval=5):
        links.append(url)
    return links

# III - Fonction de filtre des urls
def url_filter(links):
    web_links, doc_links = [], []
    for url in links:
        if url[-4:].lower() == ".pdf" or url[-5:].lower() == ".docx":
            doc_links.append(url)
        else:
            web_links.append(url)
    return web_links, doc_links

# IV - Fonction de scraper du web
def web_scraping(web_links):
    footprint = {}
    data = {}
    for url in web_links:
        article = newspaper.Article(url="%s" % (url))
        article.download()
        try:
            article.parse()
            data["text"] =  article.text
            footprint[f"{url}"] = data
        except:
            pass
    return footprint

# V - Fonction de telechargement des fichiers .pdf
def download_file(url):
    response = requests.get(url)
    if response.status_code == 200:
        document = f"assets/documents/download_{datetime.datetime.now().strftime('Day%d-Month%m-Year%Y_Hour%H-Minute%M-Second%S')}"
        if url[-4:].lower() == ".pdf":
            document = document + url[-4:]
        elif url[-5:].lower() == ".docx":
            document = document + url[-5:]
        with open(document, "wb") as file:
            file.write(response.content)
    return

# VI - Fonction pour cree un document .pdf
def create_pdf_file(txt):
    pdf = aspose.pdf.Document()
    page = pdf.pages.add()
    text = aspose.pdf.text.TextFragment(txt)
    text.text_state.font_size = 10
    text.text_state.foreground_color = aspose.pdf.Color.black
    text.text_state.font = aspose.pdf.text.FontRepository.open_font("assets/ArialCE.ttf")
    text.text_state.horizontal_alignment = aspose.pdf.HorizontalAlignment.JUSTIFY
    page.background = aspose.pdf.Color.from_cmyk(0.0, 0.0, 0.0, 0.0)
    page.paragraphs.add(text)
    pdf.save(f"assets/reports/footprint_{datetime.datetime.now().strftime('Day%d-Month%m-Year%Y_Hour%H-Minute%M-Second%S')}.pdf")
    return


# VII - Foction de formatage .pdf
def research_report(query, footprint):
    txt = f"""FOOTPRINT RESEARCH REPORT\n\n\nQuery : {query.upper()}\n\n\n"""
    i = 1
    for key, value in footprint.items():
        txt = txt + f"{i} - {key} :\n\n{value['text']}\n\n"
        i += 1
    create_pdf_file(txt)
    return txt

# VIII - Main (Programme Principal)
def main(query):
    links = url_scraping(query)
    web_links, doc_links = url_filter(links)
    footprint = web_scraping(web_links)
    description = research_report(query, footprint)
    for url in doc_links:
        download_file(url)
    return description

# Inference
#query = "Le web est riche en données, et en sachant les exploiter, elles peuvent produire des valeurs importantes. Le présent projet s’est focalisé là-dessus en mettant sur pieds une plateforme de recherche d’informations sur des cibles. Celle-ci va explorer et traiter les textes structurés des e-journaux en ligne."
#print(f"Longueur de la requete : {len(query)}")
#main(query)