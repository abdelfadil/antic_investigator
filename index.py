#coding:utf-8
import cgi
print("Content-type: text/html; charset=utf-8\n")
html = """
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
    	<meta content="width=device-width, initial-scale=1.0" name="viewport">
		<title>http://antic.search</title>
		<meta content="" name="description">
    	<meta content="" name="keywords">
		<link rel='icon' type='image/png' href='assets/img/favicon.png'>
		<link rel='stylesheet' type='text/css' href='css/style.css'>
		<link rel="preconnect" href="https://fonts.googleapis.com">
		<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
		<link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,600;1,700&family=Montserrat:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&family=Raleway:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&display=swap" rel="stylesheet">
		<link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
		<link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
		<link href="assets/vendor/aos/aos.css" rel="stylesheet">
		<link href="assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
		<link href="assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">
		<link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet">
		<link href="assets/css/style.css" rel="stylesheet">
	</head>
	<body>
		<section id='hero' class='hero'>
			<div class='container'>
				<header class='header'>
					<img src='assets/img/logo.jpg' alt='' class='logo'>
				</header>
				<h1>Do a search on a target ? Throw it there!</h1>
				<h2>ANTIC Footprinting Investigation !</h2>
				<form method='POST' action='report.py'>
					<input type='text' name='query' placeholder='Search name, Username, e-mail' required=''/>
					<input type='submit' value='Start search'>
				</form>
			</div>
		</section>
		<script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
		<script src="assets/vendor/aos/aos.js"></script>
		<script src="assets/vendor/glightbox/js/glightbox.min.js"></script>
		<script src="assets/vendor/swiper/swiper-bundle.min.js"></script>
		<script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
		<script src="assets/vendor/php-email-form/validate.js"></script>
		<script src="assets/js/main.js"></script>
	</body>
</html>
"""
print(html)